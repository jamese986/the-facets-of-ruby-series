# chomp_a_few_things.rb

puts 'Hello What\'s your first name'
name = gets.chomp
puts 'Do you have a middle name'
middle_name = gets.chop
puts 'Hello What is your last name'
last_name = gets.chomp

puts "your full name is " + name + middle_name + last_name

# Bigger better number
puts name + "\n" + 'What\'s your favourite number'
number = gets.chomp
my_number = number.to_i + 1
puts 'Your favourite number is ' + number
puts 'My favourite number is ' + my_number.to_s + ' A bigger and better number'