# a_few_things_to_try.rb

hours_in_a_year = 24 * 365
minutes_in_a_decade = 525600 * 10

# 32,536 seconds in a year
seconds_in_a_year = 32536000
age_in_seconds =  seconds_in_a_year * 27

authors_age = 1025000000 / 32536000
authors_age_when_book_was_written = 80000000 / 3253600

puts 'How many hours are there in a year?'
puts "There are #{hours_in_a_year} hours in a year."
puts 'How many hours are there in a year?'
puts "There are #{minutes_in_a_decade} minutes in a decade."
puts "How old am i in seconds #{age_in_seconds} seconds old."
puts "The authors age when the book was written is #{authors_age_when_book_was_written} years old."
puts "The authors age now is #{authors_age} years old."
